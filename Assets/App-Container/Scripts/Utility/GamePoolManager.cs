using System;
using System.Collections;
using System.Collections.Generic;
using AFramework;
using PathologicalGames;
using UnityEngine;

public class GamePoolManager : SingletonMono<GamePoolManager>
{
    public static SpawnPool VehiclePool;
    public static SpawnPool FxPool;
    public static SpawnPool OtherPool;
    public static SpawnPool LevelPool;
    public static SpawnPool UI;

    private void Awake()
    {
        VehiclePool = CreatePool("VehiclePool");
        FxPool = CreatePool("FxPool");
        OtherPool = CreatePool("OtherPool");
        LevelPool = CreatePool("LevelPool");
        UI = CreatePool("UIPool");
    }

    private SpawnPool CreatePool(string poolName)
    {
        var p = transform.Find(poolName);
        if (p == null)
        {
            var obj = new GameObject { name = poolName };
            obj.transform.SetParent(transform);
            var pool = obj.AddComponent<SpawnPool>();
            return pool;
        }
        else
        {
            return p.GetComponent<SpawnPool>();
        }
    }

    public static Transform SpawnFx(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        var fx = FxPool.Spawn(prefab, pos, rot);
        fx.eulerAngles = prefab.transform.eulerAngles;
        fx.localScale = prefab.transform.localScale;
        return fx;
    }
}