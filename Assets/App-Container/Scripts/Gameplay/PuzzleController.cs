using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AFramework;
using com.spacepuppy.Collections;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class PuzzleController : SingletonMono<PuzzleController>
{
    [SerializeField] LevelConfig levelConfig;
    [SerializeField] PathController pathPrefab;
    [SerializeField] Transform pathContainer;
    [SerializeField] Transform boatContainer;

    [Header("Boat")]
    [SerializeField] BaseBoat boatPrefab;
    [SerializeField] float boatOffset = -40;
    [SerializeField] float boatRotateOffset = 90;
    [SerializeField] float boatStarOffset = -40;

    [SerializeField] LayerMask obstacleMask;

    private List<PathController> pathControllers = new();
    private List<BaseBoat> boats = new();

    public bool IsPlaying { get; set; }
    public bool IsRotate { get; set; }
    public LayerMask ObstacleMask { get => obstacleMask; }

    private CancellationTokenSource ctSource;
    [ShowInInspector] private SerializableDictionaryBase<TargetType, int> dictCollect = new();

    private void Awake()
    {
        ctSource = new CancellationTokenSource();
    }

    [Button]
    public async UniTask StartLevel()
    {
        IsPlaying = false;

        DespawnLevel();
        InitTargetDict();
        CreatePaths();
        await UniTask.Delay(1, cancellationToken: ctSource.Token);
        IsPlaying = true;
    }

    private async UniTask CreatePaths()
    {
        pathControllers.Clear();
        foreach (var pathInfo in levelConfig.PathInfos)
        {
            PathController controller = GamePoolManager.LevelPool.Spawn(pathPrefab.transform, pathContainer).GetComponent<PathController>();
            controller.transform.localEulerAngles = Vector3.zero;
            await controller.Load(pathInfo);
            pathControllers.Add(controller);
        }
    }

    private void InitTargetDict()
    {
        dictCollect.Clear();
        for (int i = 0; i < levelConfig.TargetInfos.Count; i++)
        {
            dictCollect[levelConfig.TargetInfos[i].TargetType] = levelConfig.TargetInfos[i].Amount;
        }
    }

    private void CreateBoats()
    {
        boats.Clear();
        for (int i = 0; i < levelConfig.TargetInfos.Count; i++)
        {
            BaseBoat boat = GamePoolManager.LevelPool.Spawn(boatPrefab.transform, boatContainer).GetComponent<BaseBoat>();
            boat.Load(levelConfig.TargetInfos[i]);
            boat.transform.localPosition = Vector3.right * (boatOffset * i + boatStarOffset);
            boat.transform.localEulerAngles = Vector3.up * boatRotateOffset;
            boat.transform.DOLocalMoveX(boatOffset * i, 2f);
            boats.Add(boat);
        }
    }

    public void Collect(TargetType target, int amount)
    {
        if (dictCollect.ContainsKey(target))
        {
            dictCollect[target] -= amount;
            if (dictCollect[target] < 0)
                dictCollect[target] = 0;
        }

        CheckResult();
    }

    public void CheckResult()
    {
        bool isWin = true;
        foreach (var item in dictCollect)
        {
            if (item.Value > 0)
                isWin = false;
        }

        Debug.Log("Win: " + isWin);
    }

    private void DespawnLevel()
    {
        GamePoolManager.LevelPool.DespawnAll();
    }

    [Button]
    public void GetConfigJson()
    {
        string json = levelConfig.ToJson();
        Debug.Log(json);
        GUIUtility.systemCopyBuffer = json;
    }
}
