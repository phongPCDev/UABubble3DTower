using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BansheeGz.BGSpline.Components;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

public class PathController : MonoBehaviour
{
    [SerializeField] CurveRender curvePrefab;
    [SerializeField] Transform start;
    [SerializeField] Transform end;
    [SerializeField] BaseVehicle vehicle;
    [SerializeField] BaseContainer container;

    [SerializeField] float vehicleAngleOffset = 0;
    [SerializeField] float containerAngleOffset = -90;
    private List<CurveRender> curves = new();

    private float totalLength;

    private PathInfo pathInfo;

    public PathInfo PathInfo { get => pathInfo; }
    public BaseContainer Container { get => container; }
    public float TotalLength { get => totalLength; }

    private CancellationTokenSource ctSource;

    public async UniTask Load(PathInfo pathInfo)
    {
        ctSource = new CancellationTokenSource();
        this.pathInfo = pathInfo;

        InitCurve();
        await InitStartEnd();
        InitVehicleAndContainer();
    }

    #region Init

    private void InitCurve()
    {
        totalLength = 0;
        curves.Clear();
        for (int i = 0; i < pathInfo.NodeGroups.Count; i++)
        {
            NodeGroup group = pathInfo.NodeGroups[i];
            CurveRender curve = GamePoolManager.LevelPool.Spawn(curvePrefab.transform, transform).GetComponent<CurveRender>();
            curve.SetupPath(group.Points);
            curve.transform.localEulerAngles = Vector3.zero;
            curves.Add(curve);
            totalLength += curve.curveLength;
        }
    }

    [Button]
    private async UniTask InitStartEnd()
    {
        await UniTask.Delay(200, cancellationToken: ctSource.Token);

        //Start
        start.position = CalcPositionByDistance(0);
        start.transform.rotation = GetQuaternionAt(0);

        //End
        end.position = CalcPositionByDistance(totalLength);
        end.transform.rotation = GetQuaternionAt(totalLength - 0.4f);
    }

    [Button]
    private void InitVehicleAndContainer()
    {
        vehicle.transform.localPosition = Vector3.zero;
        container.transform.localPosition = Vector3.zero;

        container.SetCompleted(false);
        container.ChangeMatColor(pathInfo.TargetType);
        vehicle.Load(this);
    }
    #endregion

    #region Gameplay

    public async UniTask Collect()
    {
        await UniTask.Delay(500, cancellationToken: ctSource.Token);
        Debug.Log("Collect: " + pathInfo.TargetType);
        PuzzleController.I.Collect(pathInfo.TargetType, 1);
        GamePoolManager.LevelPool.Despawn(this.transform);
    }

    #endregion

    public Quaternion GetQuaternionAt(float distance)
    {
        Vector3 position = CalcPositionByDistance(distance);
        Vector3 endPos = CalcPositionByDistance(distance + 1f);
        Vector3 endUpVector = (position - transform.position).SetZ(0).normalized;
        Vector3 endForward = (endPos - position).normalized;

        //Debug.DrawRay(position, endUpVector * 20, Color.red, 1);
        //Debug.DrawRay(position, endForward * 20, Color.green, 1);
        //Debug.DrawRay(position, Vector3.Cross(endUpVector, endForward) * 20, Color.blue, 1);

        return Quaternion.LookRotation(endForward, endUpVector);
    }


    #region Curve Utils
    public Vector3 CalcPositionByDistance(float distance)
    {
        CurveRender curve = curves[0];
        int max = curves.Count;
        for (int i = 0; i < max; i++)
        {
            curve = curves[i];
            if (distance > curve.curveLength && i < curves.Count - 1)
            {
                distance -= curve.curveLength;
            }
            else break;
        }

        return curve.bgMath.CalcPositionByDistance(distance, false);
    }

    public Vector3 CalcPositionByDistance(float distance, out int curveIndex)
    {
        curveIndex = 0;
        CurveRender curve = curves[0];
        for (int i = 0; i < curves.Count; i++)
        {
            curve = curves[i];
            if (distance > curve.curveLength && i < curves.Count - 1)
            {
                distance -= curve.curveLength;
                curveIndex = i;
            }
            else
            {
                curveIndex = i;
                break;
            }
        }

        return curve.bgMath.CalcPositionByDistance(distance, true);
    }

    public Vector3 CalcPositionAndTangentByDistance(float distance, out Vector3 tangent)
    {
        foreach (var curve in curves)
        {
            if (distance > curve.curveLength)
            {
                distance -= curve.curveLength;
                continue;
            }

            return curve.bgMath.CalcPositionAndTangentByDistance(distance, out tangent);
        }

        return curves[^1].bgMath.CalcPositionAndTangentByDistance(curves[^1].curveLength, out tangent);
    }

    public Vector3 GetAngleByDistance(float distance, float offset = 0)
    {
        foreach (var curve in curves)
        {
            if (distance > curve.curveLength)
            {
                distance -= curve.curveLength;
                continue;
            }

            return curve.bgMath.GetAngleByDistance(distance, offset);
        }

        return Vector3.zero;
    }

    public void DespawnCurves()
    {
        foreach (var curve in curves)
        {
            GamePoolManager.LevelPool.Despawn(curve.transform);
        }
    }

    #endregion
}

public static class BgCurveHelper
{
    public static Vector3 GetAngleByDistance(this BGCcMath bgMath, float distance, float offset = 0)
    {
        var tangent = bgMath.CalcTangentByDistance(distance);
        return new Vector3(0, tangent.GetAngleInDegree() + offset, 0);
    }

    public static Vector3 CalcPositionAndTangentByDistance(this BGCcMath bgMath, float distance, out Vector3 tangent)
    {
        bgMath.CalcPositionAndTangentByDistance(distance, out tangent);
        tangent.z = 0;
        return new Vector3(0, 0, tangent.GetAngleInDegree());
    }
}
