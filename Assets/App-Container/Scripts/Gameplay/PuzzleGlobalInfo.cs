using System.Collections;
using System.Collections.Generic;
using AFramework;
using UnityEngine;

public class PuzzleGlobalInfo : SingletonMono<PuzzleGlobalInfo>
{
    [SerializeField] Color[] carColors;
    [SerializeField] GameObject[] carModels;

    public Color GetTargetColor(TargetType pathType)
    {
        int type = (int)pathType;
        if (type >= 0 && type < carColors.Length)
        {
            return carColors[type];
        }
        return Color.black;
    }

    public GameObject GetCarModel(TargetType pathType)
    {
        int type = (int)pathType;
        if (type >= 0 && type < carModels.Length)
        {
            return carModels[type];
        }
        return null;
    }
}
