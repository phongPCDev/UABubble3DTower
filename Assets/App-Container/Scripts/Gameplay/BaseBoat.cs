using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBoat : MonoBehaviour
{
    [SerializeField] Transform holderParent;
    [SerializeField] Transform carHolderPrefab;
    [SerializeField] List<Transform> carHolders = new();
    [SerializeField] float holderOffset = 90;


    private TargetInfo targetInfo;

    public void Load(TargetInfo targetInfo)
    {
        carHolders.Clear();
        this.targetInfo = targetInfo;
        float center = (targetInfo.Amount - 1) / 2f;
        for (int i = 0; i < targetInfo.Amount; i++)
        {
            var carHolder = GamePoolManager.LevelPool.Spawn(carHolderPrefab, holderParent);
            carHolder.transform.localPosition = Vector3.forward * holderOffset * (i - center);
            carHolders.Add(carHolder);
        }
    }
}
