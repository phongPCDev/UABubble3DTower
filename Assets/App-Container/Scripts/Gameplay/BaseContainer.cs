using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseContainer : MonoBehaviour
{
    [SerializeField] Renderer mesh;
    [SerializeField] Transform frontDoor;

    public void SetCompleted(bool isCompleted)
    {
        frontDoor.gameObject.SetActive(isCompleted);
    }

    public void ChangeMatColor(TargetType type)
    {
        mesh.material.SetColor("_Color", PuzzleGlobalInfo.I.GetTargetColor(type));
    }
}
