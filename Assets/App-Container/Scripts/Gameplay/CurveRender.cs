using System;
using System.Collections.Generic;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using UnityEngine;

public class CurveRender : MonoBehaviour
{
    public PathController pathController;
    public BGCurve bgPath;
    public BGCcMath bgMath;
    public LineRenderer line;
    public float curveLength;
    public List<NodeInfo> points;
    [SerializeField] float towerRadius = 0;
    [SerializeField] float radOffset = 0.25f;

    public void SetupPath(List<NodeInfo> points)
    {
        this.points = points;
        bgPath.Clear();
        BGCurvePoint p = null;
        for (int index = 0, count = points.Count; index < count; ++index)
        {
            // if (index == 0)
            // {
            //     p = new BGCurvePoint(bgPath, GetPointAroundTower(points[index].Center),
            //         BGCurvePoint.ControlTypeEnum.BezierSymmetrical);
            //     p.ControlSecondWorld = GetPointAroundTower(points[index + 1].Center);
            // }
            // else if (index == count - 1)
            // {
            //     p = new BGCurvePoint(bgPath, GetPointAroundTower(points[index].Center),
            //         BGCurvePoint.ControlTypeEnum.BezierSymmetrical);
            //     p.ControlFirstWorld = GetPointAroundTower(points[index - 1].Center);
            // }
            // else if (index % 3 == 0)
            // {
            //     p = new BGCurvePoint(bgPath, GetPointAroundTower(points[index].Center),
            //         BGCurvePoint.ControlTypeEnum.BezierSymmetrical);
            //     p.ControlFirstWorld = GetPointAroundTower(points[index - 1].Center);
            //     p.ControlSecondWorld = GetPointAroundTower(points[index + 1].Center);
            // }
            p = new BGCurvePoint(bgPath, GetPointAroundTower(points[index].Center),
                   BGCurvePoint.ControlTypeEnum.BezierSymmetrical);
            p.ControlFirstWorld = GetPointAroundTower(points[index].Center);
            p.ControlSecondWorld = GetPointAroundTower(points[index].Center);

            if (p != null)
            {
                bgPath.AddPoint(p);
                p = null;
            }
        }

        for (int i = 0; i < bgPath.Points.Length; i++)
        {
            BGCurvePointI point = bgPath.Points[i];
            if (i < bgPath.Points.Length - 1)//First to near last.
            {
                point.ControlSecondWorld = Vector3.Slerp(point.PositionWorld, bgPath.Points[i + 1].PositionWorld, 0.3f);
            }
            else //Last
            {
                point.ControlFirstWorld = Vector3.Slerp(point.PositionWorld, bgPath.Points[i - 1].PositionWorld, 0.3f);
            }
        }

        bgPath.GetComponent<BGCcVisualizationLineRenderer>().UpdateUI();
        bgMath.Recalculate();
        curveLength = bgMath.GetDistance(bgPath.Points.Length - 1);
    }

    private Vector3 GetPointAroundTower(Vector3 org)
    {
        // float absX = Mathf.Abs(org.x);
        // float angle = Mathf.Atan2(GetY(org.x) * (absX > towerRadius ? -1 : 1), org.x);
        // org.y = towerRadius * Mathf.Sin(angle);
        // org.x = towerRadius * Mathf.Cos(angle) * Mathf.Sign(org.x);

        // float towerLength = towerRadius * 2 * Mathf.PI;
        // float tempX = (org.x + 2 * towerRadius);
        // float diff = (tempX / towerLength);
        // org.x = Mathf.Cos(diff) * towerRadius;
        float percent = (org.x + 2 * towerRadius) / (4 * towerRadius);
        float rad = percent * 2 * Mathf.PI;

        float x = -Mathf.Cos(rad + radOffset) * towerRadius;
        float y = Mathf.Sin(rad + radOffset) * towerRadius;

        org.x = x;
        org.y = y;

        //Debug.Log($"{absX}||{angle * Mathf.Rad2Deg}|{Mathf.Sin(angle)}");
        return org;
    }

    private float GetY(float x)
    {
        return Mathf.Sqrt(towerRadius * towerRadius - x * x);
    }

    public NodeInfo GetNodeInfo(int pointIndex)
    {
        if (pointIndex == 0)
        {
            return points[0];
        }
        else if (pointIndex == points.Count - 1)
        {
            return points[pointIndex * 3];
        }
        else
        {
            return points[pointIndex * 3];
        }
    }

    public List<NodeInfo> GetListNodeInfo(int pointIndex)
    {
        if (pointIndex == 0)
        {
            return new List<NodeInfo> { points[0], points[0], points[1] };
        }
        else if (pointIndex == points.Count - 1)
        {
            var center = pointIndex * 3;
            return new List<NodeInfo> { points[center - 1], points[center], points[center + 1] };
        }
        else
        {
            var center = pointIndex * 3;
            return new List<NodeInfo> { points[center - 1], points[center], points[center] };
        }
    }
}