﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
#if USE_SPINE
using Spine.Unity;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public static class GameHelper
{
    #region Int Extensions

    public static int FloorAt0(this int origin)
    {
        return origin.FloorAt(0);
    }

    public static int FloorAt(this int origin, int clampValue)
    {
        return Mathf.Max(origin, clampValue);
    }

    public static string FormatToNumberAndCharacter(this int origin)
    {
        if (origin < 10000)
            return origin.ToString("N0"); // No formatting needed for numbers less than 10000

        string[] suffixes = { "", "K", "M", "B", "T" }; // Suffixes for thousand, million, billion, trillion
        int suffixIndex = 0;

        double num = origin;

        while (num >= 1000 && suffixIndex < suffixes.Length - 1)
        {
            num /= 1000;
            suffixIndex++;
        }

        num = Math.Floor(num * 10) / 10;

        return $"{num}{suffixes[suffixIndex]}"; // Formats with one decimal place if needed
    }
    #endregion

    #region Vecter2 Extentions

    public static float GetAngleInRadian(this Vector2 v1, Vector2 v2)
    {
        return Mathf.Atan2(v2.y - v1.y, v2.x - v1.x);
    }

    public static Vector2 Rotate(this Vector2 v, float angle)
    {
        v = Quaternion.AngleAxis(angle, Vector2.up) * v;
        return v;
    }


    public static Vector2 SetX(this Vector2 vec, float newX)
    {
        vec.x = newX;
        return vec;
    }


    public static Vector2 SetY(this Vector2 vec, float newY)
    {
        vec.y = newY;
        return vec;
    }

    #endregion

    #region Vecter3 Extentions

    public static float GetAngleInRadian(this Vector3 v1, Vector3 v2)
    {
        return Mathf.Atan2(v2.y - v1.y, v2.x - v1.x);
    }

    public static float GetAngleInDegree(this Vector3 v1, Vector3 v2)
    {
        return Mathf.Atan2(v2.z - v1.z, v2.x - v1.x) * Mathf.Rad2Deg;
    }

    public static float GetAngleInDegree(this Vector3 v1)
    {
        return Mathf.Atan2(v1.x, v1.z) * Mathf.Rad2Deg;
    }

    public static float GetAngleInDegree(this Vector2 v1, Vector2 v2)
    {
        return Mathf.Atan2(v1.y - v2.y, v1.x - v2.x) * Mathf.Rad2Deg;
    }

    public static Vector3 Rotate(this Vector3 v, float angle)
    {
        v = Quaternion.AngleAxis(angle, Vector3.back) * v;
        return v;
    }

    public static bool AreLinesIntersecting(Transform p1, Transform q1, Transform p2, Transform q2, bool isDrawLines = false)
    {
        return AreLinesIntersecting(p1.position, q1.position, p2.position, q2.position, isDrawLines);
    }

    public static bool AreLinesIntersecting(Vector3 p1, Vector3 q1, Vector3 p2, Vector3 q2, bool isDrawLines = false)
    {
        if (isDrawLines)
        {
            Debug.DrawLine(p1, q1, Color.yellow, 0.5f);
            Debug.DrawLine(p2, q2, Color.yellow, 0.5f);
        }

        Vector3 direction1 = q1 - p1;
        Vector3 direction2 = q2 - p2;
        Vector3 crossProduct = Vector3.Cross(direction1, direction2);
        float crossMagnitudeSquared = crossProduct.sqrMagnitude;
        if (crossMagnitudeSquared < Mathf.Epsilon)
        {
            // Các đoạn thẳng song song (có thể đồng phẳng hoặc không).
            return false;
        }
        Vector3 diff = p2 - p1;
        float t = Vector3.Dot(Vector3.Cross(diff, direction2), crossProduct) / crossMagnitudeSquared;
        float u = Vector3.Dot(Vector3.Cross(diff, direction1), crossProduct) / crossMagnitudeSquared;
        // Kiểm tra xem tham số t và u có nằm trong khoảng [0, 1] hay không
        // để xác định đoạn thẳng thực sự giao nhau trong đoạn thẳng.
        if (t >= 0 && t <= 1 && u >= 0 && u <= 1)
        {
            return true;
        }

        return false;
    }

    public static Vector3 SetX(this Vector3 vec, float newX)
    {
        vec.x = newX;
        return vec;
    }


    public static Vector3 SetY(this Vector3 vec, float newY)
    {
        vec.y = newY;
        return vec;
    }

    public static Vector3 SetZ(this Vector3 vec, float newZ)
    {
        vec.z = newZ;
        return vec;
    }

    #endregion

    #region Transform Extentions

    public static List<Transform> GetAllChilds(this Transform t)
    {
        List<Transform> lstTransforms = new List<Transform>();
        int childCount = t.childCount;
        for (int i = 0; i < childCount; i++)
            lstTransforms.Add(t.GetChild(i));

        return lstTransforms;
    }

    public static void DeleteAllChilds(this Transform t)
    {
        int childCount = t.childCount;
        for (int i = 0; i < childCount; i++)
            Object.DestroyImmediate(t.GetChild(0).gameObject);
    }

    public static void OrderByY(this Transform t)
    {
        var position = t.position;
        position.z = position.y * 0.01f;
        t.position = position;
    }

    public static void SetSizeByWidth(this RectTransform rect, float width, float aspect)
    {
        rect.sizeDelta = new Vector2(width, width * aspect);
    }

    public static void SetSizeByHeight(this RectTransform rect, float height, float aspect)
    {
        rect.sizeDelta = new Vector2(height / aspect, height);
    }

    public static void SetSize(this RectTransform rect, Vector2 size)
    {
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
    }

    #endregion

    #region Image Extentions

    public static void SetSizeByWidth(this Image img, float width)
    {
        if (img.sprite == null)
            return;
        var sprite = img.sprite;
        float aspect = sprite.bounds.size.y / sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(width, width * aspect);
    }

    public static void SetSizeByHeight(this Image img, float height)
    {
        if (img.sprite == null)
            return;
        var sprite = img.sprite;
        float aspect = sprite.bounds.size.y / sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(height / aspect, height);
    }

    public static void SetAlpha(this Image img, float alpha)
    {
        Color color = img.color;
        color.a = alpha;
        img.color = color;
    }

    public static void SetAlpha(this TMPro.TextMeshProUGUI text, float alpha)
    {
        Color color = text.color;
        color.a = alpha;
        text.color = color;
    }

    #endregion

    #region AsssetDatabase Helper

    public static void SetDirty(Object obj)
    {
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(obj);
#endif
    }

    public static void SaveAssetDatabase(Object obj, bool isRefresh = false)
    {
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(obj);
        UnityEditor.AssetDatabase.SaveAssets();
        if (isRefresh)
            UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public static List<T> GetAllAssetAtPath<T>(string filter, string path)
    {
#if UNITY_EDITOR
        string[] findAssets = UnityEditor.AssetDatabase.FindAssets(filter, new[] { path });
        List<T> os = new List<T>();
        foreach (var findAsset in findAssets)
        {
            os.Add((T)Convert.ChangeType(
                UnityEditor.AssetDatabase.LoadAssetAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(findAsset),
                    typeof(T)), typeof(T)));
        }

        return os;
#endif
        return null;
    }

    public static List<Object> GetAllAssetsAtPath(string path)
    {
#if UNITY_EDITOR
        string[] paths = { path };
        var assets = UnityEditor.AssetDatabase.FindAssets(null, paths);
        var assetsObj = assets.Select(s =>
            UnityEditor.AssetDatabase.LoadMainAssetAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
#endif
        return null;
    }

    public static List<Sprite> GetAllSpriteAssetsAtPath(string path)
    {
#if UNITY_EDITOR
        string[] paths = { path };
        var assets = UnityEditor.AssetDatabase.FindAssets("t:sprite", paths);
        var assetsObj = assets.Select(s =>
            UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
#endif
        return null;
    }

    public static List<Material> GetAllMaterialAssetsAtPath(string path)
    {
#if UNITY_EDITOR
        string[] paths = { path };
        var assets = UnityEditor.AssetDatabase.FindAssets("t:material", paths);
        var assetsObj = assets.Select(s =>
            UnityEditor.AssetDatabase.LoadAssetAtPath<Material>(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
#endif
        return null;
    }

    #endregion

    #region List Extentions

    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    // public static void Shuffle2<T>(this IList<T> ts)
    // {
    //     var count = ts.Count;
    //     var last = count - 1;
    //     for (var i = 0; i < last; ++i)
    //     {
    //         var r = MyRandom.Range(i, count);
    //         var tmp = ts[i];
    //         ts[i] = ts[r];
    //         ts[r] = tmp;
    //     }
    // }

    public static bool IsIndexOutOfList<T>(this IList<T> list, int index)
    {
        return (index < 0) || (index >= list.Count);
    }

    public static T RandomItemWithPercent<T>(this IList<T> list, IList<float> percentList, int defaultIndex = 0)
    {
        T result = list.IsIndexOutOfList(defaultIndex) ? default(T) : list[defaultIndex];
        float percentValue = UnityEngine.Random.Range(0f, 100f);
        float curPercent = 0f;

        for (int index = 0; index < list.Count; index++)
        {
            curPercent += percentList.IsIndexOutOfList(index) ? 0f : percentList[index];
            if (percentValue <= curPercent)
            {
                result = list[index];
                break;
            }
        }

        return result;
    }

    #endregion

    #region Random Extention

    public static float GetRandom1(float min, float max)
    {
        return UnityEngine.Random.Range(min, max) * (UnityEngine.Random.Range(0, 2) == 1 ? 1 : -1);
    }

    #endregion

    #region Scene Extention

    public static void DeleteScene(string sceneName)
    {
#if UNITY_EDITOR
        var scene = GetAllScenes().Find(s => s == sceneName);
        if (scene != null)
        {
            var lstObjs = SceneManager.GetSceneByName(scene).GetRootGameObjects();
            foreach (var gameObject in lstObjs)
                GameObject.Destroy(gameObject);
        }
#endif
    }

    public static List<string> GetAllScenes()
    {
        List<string> scenes = new List<string>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
            scenes.Add(SceneManager.GetSceneAt(i).name);
        return scenes;
    }

    #endregion

#if USE_SPINE

    public static string GetSkinNameAtIndex(this SkeletonDataAsset spine, int index)
    {
        return spine.GetSkeletonData(true).Skins.Items[index].Name;
    }

    public static void SetSkin(this SkeletonGraphic spine, string skinName)
    {
        spine.Skeleton.SetSkin(skinName);
        spine.Skeleton.SetSlotsToSetupPose();
        spine.AnimationState.Apply(spine.Skeleton);
    }

    public static void SetSkin(this SkeletonAnimation spine, string skinName)
    {
        spine.Skeleton.SetSkin(skinName);
        spine.Skeleton.SetSlotsToSetupPose();
        spine.AnimationState.Apply(spine.Skeleton);
    }

    public static float GetDuration(this SkeletonAnimation spine, string animName)
    {
        return spine.SkeletonDataAsset.GetSkeletonData(true).Animations.Find(s => s.Name == animName).Duration;
    }
#endif

    public static Vector2 GetPivot(this Sprite s)
    {
        return new Vector2(s.pivot.x / s.rect.width, s.pivot.y / s.rect.height);
    }

    public static void SetPivot(GameObject o)
    {
        var sprite = o.GetComponent<Image>().sprite;
        var rect = o.GetComponent<RectTransform>();
        Vector2 curPivot = rect.pivot;
        rect.pivot = sprite.GetPivot();
        Vector2 delta = curPivot - rect.pivot;
        rect.anchoredPosition -= new Vector2(rect.sizeDelta.x * delta.x, rect.sizeDelta.y * delta.y);
    }

    public static void SetAlpha(this SpriteRenderer sprite, float alpha)
    {
        var color = sprite.color;
        color.a = alpha;
        sprite.color = color;
    }

    #region Random Percent Helper

    public static T GetRandomValueBasedOnPercent<T>(params (float, T)[] list)
    {
        float percentValue = UnityEngine.Random.Range(0f, 100f);
        float curPercent = 0f;

        for (int i = 0; i < list.Length; i++)
        {
            curPercent += list[i].Item1;
            if (percentValue < curPercent)
            {
                return list[i].Item2;
            }
        }

        return list[list.Length - 1].Item2;
    }

    public static T GetRandomValueWithSamePercent<T>(params T[] list)
    {
        if (list.Length == 0) return default(T);

        float step = 100f / list.Length;
        float percentValue = UnityEngine.Random.Range(0f, 100f);
        float curPercent = 0f;

        for (int i = 0; i < list.Length; i++)
        {
            curPercent += step;
            if (percentValue < curPercent)
            {
                return list[i];
            }
        }

        return list[list.Length - 1];
    }

    public static int GetRandomIndexBasedOnPercent(IList<float> perCentList)
    {
        float percentValue = UnityEngine.Random.Range(0f, 100f);
        float curPercent = 0f;

        for (int i = 0; i < perCentList.Count; i++)
        {
            int index = i;
            curPercent += perCentList[index];
            if (percentValue < curPercent)
            {
                return index;
            }
        }

        return perCentList.Count - 1;
    }

    #endregion

    public static Vector3 CalculateThirdVertex(Vector3 vertex1, Vector3 vertex2, float angleDegrees)
    {
        // Chuyển đổi góc từ độ sang radian
        float angleRadians = angleDegrees * Mathf.Deg2Rad;

        // Tính vector từ đỉnh 1 đến đỉnh 2
        Vector3 vertex1ToVertex2 = (vertex2 - vertex1) / 2f;

        // Tính vector mới bằng cách xoay vector từ đỉnh 1 đến đỉnh 2
        Vector3 rotatedVector = Quaternion.Euler(0, 0, angleDegrees) * vertex1ToVertex2;

        // Tính điểm thứ ba bằng cách cộng vector xoay với đỉnh 1
        Vector3 thirdVertex = vertex1 + rotatedVector;

        return thirdVertex;
    }

    public static Texture2D[] GetTextureOfAtlas(UnityEngine.U2D.SpriteAtlas spriteAtlas)
    {
#if UNITY_EDITOR
        var method = typeof(UnityEditor.U2D.SpriteAtlasExtensions).GetMethod("GetPreviewTextures", BindingFlags.Static | BindingFlags.NonPublic);
        object obj = method.Invoke(null, new object[] { spriteAtlas });
        Texture2D[] textures = obj as Texture2D[];
        return textures;
#endif
        return null;
    }

    public static Vector2 GetDirect(float value)
    {
        float radian = Mathf.Deg2Rad * value;
        return new Vector2(Mathf.Cos(radian), MathF.Sin(radian));
    }

    public static string GetNumberFormat(this int value)
    {
        return value.ToString("N0");
    }

    public static string TimeFormatFollowDay(float time)
    {
        int timeInt = Mathf.CeilToInt(time);
        int day = timeInt / 86400;
        int hour = (timeInt % 86400) / 3600;
        int minute = (timeInt % 3600) / 60;
        int second = timeInt - hour * 3600 - minute * 60;

        if (day > 0)
        {
            return string.Format("{0:00}d:{1:00}h:{2:00}", day, hour, minute);
        }
        else if (hour > 0)
        {
            return string.Format("{0:00}h:{1:00}m:{2:00}", hour, minute, second);
        }
        else
        {
            return string.Format("{0:00}:{1:00}", minute, second);
        }
    }

    public static Vector3 RoundVector(this Vector3 vector)
    {
        return new Vector3(
            (float)Math.Round(vector.x * 1000) / 1000f,
            (float)Math.Round(vector.y * 1000) / 1000f,
            (float)Math.Round(vector.z * 1000) / 1000f
        );
    }

    public static Sprite ToSprite(this Texture2D tex, float pixelPerUnit)
    {
        return Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), pixelPerUnit);
    }

    public static float time;

    public static void StartTime()
    {
        time = Time.realtimeSinceStartup;
    }

    public static void LogProcessTime(string content)
    {
        Debug.Log(content + (Time.realtimeSinceStartup - time));
    }

    public static bool Contains(this LayerMask mask, int layer)
    {
        return (mask & (1 << layer)) != 0;
    }
}