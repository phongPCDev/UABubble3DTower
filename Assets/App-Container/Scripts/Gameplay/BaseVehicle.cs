using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class BaseVehicle : MonoBehaviour
{
    [SerializeField] Transform modelParent;
    [SerializeField] float moveForwardSpeed = 1f;
    [SerializeField] float moveBackSpeed = 1f;

    private GameObject model;
    private TargetType type;
    private PathController path;
    private float targetDistance;
    private float distance;
    private CarState state;

    public void Load(PathController path)
    {
        this.path = path;
        this.type = path.PathInfo.TargetType;
        targetDistance = distance = 0;

        if (model != null && GamePoolManager.VehiclePool.IsSpawned(model.transform))
        {
            GamePoolManager.VehiclePool.Despawn(model.transform);
        }

        model = GamePoolManager.VehiclePool.Spawn(PuzzleGlobalInfo.I.GetCarModel(type), modelParent.position, Quaternion.identity, modelParent).gameObject;
        model.transform.localEulerAngles = Vector3.zero;
    }

    private void Update()
    {
        UpdateState();
    }

    private void UpdateState()
    {
        switch (state)
        {
            case CarState.Idle:
                distance = targetDistance;
                break;
            case CarState.Forward:
                distance += Time.deltaTime * moveForwardSpeed;
                transform.position = path.CalcPositionByDistance(distance);
                transform.rotation = path.GetQuaternionAt(distance);
                if (IsNear(distance, targetDistance) || distance >= targetDistance)
                    state = CarState.Idle;
                break;
            case CarState.Backward:
                distance -= Time.deltaTime * moveBackSpeed;
                transform.position = path.CalcPositionByDistance(distance);
                transform.rotation = path.GetQuaternionAt(distance);
                if (IsNear(distance, targetDistance) || distance <= targetDistance)
                    state = CarState.Idle;
                break;
            default:
                break;
        }
    }

    private bool IsNear(float a, float b)
    {
        return Mathf.Abs(a - b) <= Mathf.Epsilon;
    }

    public virtual void StartMoveToContainer()
    {
        if (state == CarState.Forward)
            return;

        targetDistance = path.TotalLength - 0.4f;
        state = CarState.Forward;
    }

    public virtual void StartMoveBack()
    {
        if (state == CarState.Backward)
            return;

        targetDistance = 0;
        state = CarState.Backward;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!PuzzleController.I.IsPlaying || path == null)
            return;

        if (PuzzleController.I.ObstacleMask.Contains(other.gameObject.layer))
        {
            Debug.Log(other.gameObject.name + "| " + other.gameObject.layer);
            BaseContainer container = other.gameObject.GetComponent<BaseContainer>();
            if (container != null)
            {
                if (container == path.Container)
                {
                    path.Collect();
                }
                else
                {
                    StartMoveBack();
                }
            }
        }
    }


    public enum CarState
    {
        Idle,
        Forward,
        Backward,
    }
}
