using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LevelConfig
{
    [SerializeField] List<PathInfo> pathInfos = new();
    [SerializeField] List<TargetInfo> targetInfos = new();
    public List<PathInfo> PathInfos { get => pathInfos; set => pathInfos = value; }
    public List<TargetInfo> TargetInfos { get => targetInfos; set => targetInfos = value; }

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }
}

[Serializable]
public class PathInfo
{
    [SerializeField] List<NodeGroup> nodeGroups = new();
    [SerializeField] TargetType targetType;

    public TargetType TargetType { get => targetType; set => targetType = value; }
    public List<NodeGroup> NodeGroups { get => nodeGroups; set => nodeGroups = value; }
}

[Serializable]
public struct NodeGroup
{
    [SerializeField] List<NodeInfo> points;
    public List<NodeInfo> Points { get => points; set => points = value; }
}

[Serializable]
public struct NodeInfo
{
    public Vector3 Center;
    public Vector3[] Controls;
}

[Serializable]
public struct TargetInfo
{
    public TargetType TargetType;
    public int Amount;
}

[Serializable]
public enum TargetType
{
    None = -1,
    Green,
    Yellow,
}
