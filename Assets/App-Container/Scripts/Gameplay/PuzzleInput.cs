using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleInput : MonoBehaviour
{
    [Header("Layer")]
    [SerializeField] LayerMask vehicleMask;
    [SerializeField] LayerMask towerMask;

    [Header("Rotate")]
    [SerializeField] Transform rotateTrans;

    [Header("Camera")]
    public float RotateSpeed = 1;
    public float LerpSpeed = 2;
    public float DistanceRotateThreshold = 2;

    private Camera mainCam;
    private Vector3 startPos = Vector3.zero;
    private Vector3 prevPos = Vector3.zero;
    private Vector3 targetPos = Vector3.zero;
    private Vector3 posDelta = Vector3.zero;
    private bool rotationBegan = false;
    private Vector3 horizontalRotateAxis = Vector3.zero;

    private void Awake()
    {
        mainCam = Camera.main;
        horizontalRotateAxis = Vector3.Cross(this.transform.position - mainCam.transform.position, mainCam.transform.right).normalized;
    }

    private void Start()
    {
        mainCam = Camera.main;
    }
    private void Update()
    {
        if (!PuzzleController.I.IsPlaying)
            return;

        if (Input.GetMouseButtonUp(0))
            CastForVehicle();

        Rotate();
    }

    private void CastForVehicle()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 500, Color.green, 1);
        bool isHit = Physics.Raycast(ray.origin, ray.direction, out RaycastHit hitInfo, Mathf.Infinity, vehicleMask, QueryTriggerInteraction.Collide);
        if (isHit)
        {
            BaseVehicle vehicle = hitInfo.collider.gameObject.GetComponent<BaseVehicle>();
            vehicle.StartMoveToContainer();
        }
        Debug.Log($"Casted: {isHit}");
    }

    private void Rotate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rotationBegan = false;
            startPos = Input.mousePosition;
            prevPos = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            targetPos = Input.mousePosition;
        }

        if (!rotationBegan)
        {
            if (Vector3.Distance(startPos, targetPos) >= DistanceRotateThreshold)
            {
                rotationBegan = true;
                PuzzleController.I.IsRotate = true;
            }
        }
        else if (prevPos != targetPos)
        {
            posDelta = targetPos - prevPos;
            rotateTrans.Rotate(rotateTrans.transform.forward, -Vector3.Dot(posDelta, mainCam.transform.right) * RotateSpeed * Time.deltaTime, Space.World);
            prevPos = Vector3.Lerp(prevPos, targetPos, LerpSpeed * Time.deltaTime);
        }
    }

}
