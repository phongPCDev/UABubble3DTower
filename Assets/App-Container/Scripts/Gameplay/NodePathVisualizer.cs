using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class NodePathVisualizer : MonoBehaviour
{
    [ShowInInspector] List<List<Transform>> transGroups = new();
    [SerializeField] PathInfo pathInfo;
    [SerializeField] bool useLocal = false;
    [SerializeField] PathController pathPrefab;

    private PathController path;

    [Button]
    public void GetNodes()
    {
        pathInfo = new PathInfo();
        pathInfo.NodeGroups.Clear();
        pathInfo.NodeGroups.Capacity = transGroups.Count;

        for (int i = 0; i < transGroups.Count; i++)
        {
            List<NodeInfo> nodeInfos = new List<NodeInfo>();
            List<Transform> transforms = transGroups[i];
            for (int j = 0; j < transforms.Count; j++)
            {
                Transform trans = transforms[j];
                if (trans != null)
                {
                    NodeInfo nodeInfo = new NodeInfo() { Center = useLocal ? trans.localPosition : trans.position };
                    nodeInfos.Add(nodeInfo);
                }
            }
            pathInfo.NodeGroups.Add(new NodeGroup() { Points = nodeInfos });
        }
    }

    [Button]
    public void DrawCurve()
    {
        if (pathPrefab)
        {
            if (path)
            {
                path.DespawnCurves();
                GamePoolManager.OtherPool.Despawn(path.transform);
            }
            
            var curve = GamePoolManager.OtherPool.Spawn(pathPrefab.transform, transform).GetComponent<PathController>();
            curve.Load(pathInfo);
            path = curve;
        }
    }
}
