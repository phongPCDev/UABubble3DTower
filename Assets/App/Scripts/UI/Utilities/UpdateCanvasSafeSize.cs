﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateCanvasSafeSize : MonoBehaviour
{
    protected const float ConformPercentage = 0.6f;

    protected RectTransform Panel;
    protected Rect LastSafeArea = new Rect(0, 0, 0, 0);

    protected virtual void Awake()
    {
        Panel = GetComponent<RectTransform>();
        Refresh();
    }

    // private void Update()
    // {
    //     Refresh();
    // }

    protected void Refresh()
    {
        Rect safeArea = GetSafeArea();

        if (safeArea != LastSafeArea)
            ApplySafeArea(safeArea);
    }

    private Rect GetSafeArea()
    {
        return Screen.safeArea;
    }

    protected virtual void ApplySafeArea(Rect r)
    {
        LastSafeArea = r;

        // Convert safe area rectangle from absolute pixels to normalised anchor coordinates
        Vector2 anchorMin = r.position;
        Vector2 anchorMax = r.position + r.size;

        anchorMin.x /= Screen.width;
        anchorMin.x *= ConformPercentage;

        anchorMin.y /= Screen.height;
        anchorMin.y *= ConformPercentage;

        anchorMax.x /= Screen.width;
        anchorMax.x = 1.0f - (1.0f - anchorMax.x) * ConformPercentage;

        anchorMax.y /= Screen.height;
        anchorMax.y = 1.0f - (1.0f - anchorMax.y) * ConformPercentage;

        Panel.anchorMin = anchorMin;
        Panel.anchorMax = anchorMax;

        // ADebug.LogFormat("New safe area applied to {0}: x={1}, y={2}, w={3}, h={4} on full extents w={5}, h={6}",
        //     name, r.x, r.y, r.width, r.height, Screen.width, Screen.height);
    }
}