using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseOffIconButtonToggle : BaseButtonToggle, IButtonToggleBehavior
{
    [SerializeField] private GameObject OffIcon;

    protected override void Awake()
    {
        base.Awake();

        SetButtonToggleBehavior(this);
    }

    public void SetState(bool isOn)
    {
        OffIcon.SetActive(!isOn);
    }
}
