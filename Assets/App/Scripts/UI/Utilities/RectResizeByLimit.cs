using System.Collections;
using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;

public class RectResizeByLimit : MonoBehaviour
{
    [SerializeField] RectTransform limitRect;
    [SerializeField] bool isMoveX = true;
    [SerializeField] bool isMoveY;

    private RectTransform rect;
    private Vector2 sizeDelta;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        sizeDelta = rect.sizeDelta;
    }

    private void OnEnable()
    {
        if (isMoveX)
        {
            Vector3 rectPos = RectTransformUtility.WorldToScreenPoint(Camera.main, rect.position);
            Vector3 limitPos = RectTransformUtility.WorldToScreenPoint(Camera.main, limitRect.position);
            if (rectPos.x + sizeDelta.x > limitPos.x)
            {
                rect.sizeDelta = sizeDelta + Vector2.right * (limitPos.x - (rectPos.x + sizeDelta.x));
            }
        }
    }
}
