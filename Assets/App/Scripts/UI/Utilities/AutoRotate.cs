﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour
{
    public float Speed = 300;
    public bool UseUnScaledTime = false;
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }

    void Update()
    {
        if (UseUnScaledTime) _transform.eulerAngles += Vector3.back * Speed * Time.unscaledDeltaTime;
        else _transform.eulerAngles += Vector3.back * Speed * Time.deltaTime;
    }
}