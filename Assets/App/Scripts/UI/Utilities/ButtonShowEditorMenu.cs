﻿using AFramework.UI;
using UnityEngine;
using UnityEngine.UI;

public class ButtonShowEditorMenu : MonoBehaviour
{
    private void Awake()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || USE_CHEAT
        GetComponent<Button>().onClick.AddListener(OnClicked);
#else
        if (!SettingPopup.IsTestMode)
        {
            Destroy(gameObject);
        }
        else
        {
            GetComponent<Button>().onClick.AddListener(OnClicked);
        }
#endif
    }

    private void OnClicked()
    {
        CanvasManager.Push("GameEditorMenu", null);
    }
}