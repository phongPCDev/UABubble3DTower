﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class ButtonToggle : MonoBehaviour
{
    // public Material Gray;

    #region Variable.

    [Header("Game Object")] public GameObject OffIcon;

    [HideInInspector] public UnityEvent OnClick;

    [Header("UGUI Text")] public Text textState;

    [Header("Image")] [SerializeField] private Image _img;
    [Header("Image")] [SerializeField] private Image _imgBg;

    [Header("Sprite")] [SerializeField] private Sprite spriteOn;
    [SerializeField] private Sprite spriteOff;

    [Header("Group")]
    public GameObject groupOn;
    public GameObject groupOff;

    [Header("Boolean")] public bool IsUseTextState = false;
    public bool IsUseTextAndColor = false;
    public bool IsUseOffIcon = false;
    public bool IsColorIcon = false;
    public bool IsUseGroup = false;

    public Image ButtonBg { get; private set; } = null;
    #endregion

    #region Functions.

    private void Awake()
    {
        if (_img == null)
            _img = GetComponent<Image>();
        Button button = GetComponent<Button>();
        if (button == null) button = gameObject.AddComponent<Button>();
        button.onClick.AddListener(Click);
        ButtonBg = button.image;
        _imgBg = GetComponent<Image>();
    }

    private void Click()
    {
        OnClick?.Invoke();
    }

    public void SetState(bool isOn)
    {
        if (IsUseTextState)
        {
            textState.text = isOn ? "ON" : "OFF";
        }
        else if (IsUseOffIcon)
        {
            OffIcon.SetActive(!isOn);
        }
        else if (IsUseTextAndColor)
        {
            textState.text = isOn ? "ON" : "OFF";
            // _img.color = isOn ? Color.white : Color.gray;
        }
        else if (IsColorIcon)
        {
            // _img.color = isOn ? Color.white : Color.gray;
        }
        else if (IsUseGroup)
        {
            groupOn.SetActive(isOn);
            groupOff.SetActive(!isOn);
        }
        else
        {
            _img.sprite = isOn ? spriteOn : spriteOff;
            // _imgBg.material = isOn ? null : Gray;
            //_img.SetNativeSize();
        }
    }

    #endregion
}