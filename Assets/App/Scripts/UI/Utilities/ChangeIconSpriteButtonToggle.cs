using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeIconSpriteButtonToggle : BaseButtonToggle, IButtonToggleBehavior
{
    [SerializeField] private Image iconImage;
    [SerializeField] private Sprite spriteOn;
    [SerializeField] private Sprite spriteOff;

    protected override void Awake()
    {
        base.Awake();

        SetButtonToggleBehavior(this);
    }

    public void SetState(bool isOn)
    {
        iconImage.sprite = isOn ? spriteOn : spriteOff;
    }
}
