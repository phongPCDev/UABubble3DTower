using UnityEngine;

public class UpdateCustomCanvasSafeSize : UpdateCanvasSafeSize
{
    [SerializeField] bool ConformXMin = true;  // Conform to screen safe area on X-axis min (default true, disable to ignore)
    [SerializeField] bool ConformXMax = true;  // Conform to screen safe area on X-axis max (default true, disable to ignore)
    [SerializeField] bool ConformYMin = true;  // Conform to screen safe area on Y-axis min (default true, disable to ignore)
    [SerializeField] bool ConformYMax = true;  // Conform to screen safe area on Y-axis max (default true, disable to ignore)

    Vector2 originAnchorMin;
    Vector2 originAnchorMax;

    protected override void Awake()
    {
        Panel = GetComponent<RectTransform>();
        originAnchorMin = Panel.anchorMin;
        originAnchorMax = Panel.anchorMax;
        Refresh();
    }

    protected override void ApplySafeArea(Rect r)
    {
        LastSafeArea = r;

        // Convert safe area rectangle from absolute pixels to normalised anchor coordinates
        Vector2 anchorMin = r.position;
        Vector2 anchorMax = r.position + r.size;

        anchorMin.x /= Screen.width;
        anchorMin.x *= ConformPercentage;

        anchorMin.y /= Screen.height;
        anchorMin.y *= ConformPercentage;

        anchorMax.x /= Screen.width;
        anchorMax.x = 1.0f - (1.0f - anchorMax.x) * ConformPercentage;

        anchorMax.y /= Screen.height;
        anchorMax.y = 1.0f - (1.0f - anchorMax.y) * ConformPercentage;

        Panel.anchorMin = new Vector2(ConformXMin ? anchorMin.x : originAnchorMin.x, ConformYMin ? anchorMin.y : originAnchorMin.y);
        Panel.anchorMax = new Vector2(ConformXMax ? anchorMax.x : originAnchorMax.x, ConformYMax ? anchorMax.y : originAnchorMax.y);

        // ADebug.LogFormat("New safe area applied to {0}: x={1}, y={2}, w={3}, h={4} on full extents w={5}, h={6}",
        //     name, r.x, r.y, r.width, r.height, Screen.width, Screen.height);
    }
}