using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BaseButtonToggle : MonoBehaviour
{
    [HideInInspector] public UnityEvent OnClick;

    public Image ButtonBg { get; private set; } = null;

    private IButtonToggleBehavior buttonToggleBehavior = null;

    protected virtual void Awake()
    {
        Button button = GetComponent<Button>();
        if (button == null) button = gameObject.AddComponent<Button>();
        button.onClick.AddListener(Click);
        ButtonBg = button.image;
    }

    protected void SetButtonToggleBehavior(IButtonToggleBehavior buttonToggleBehavior)
    {
        this.buttonToggleBehavior = buttonToggleBehavior;
    }

    private void Click()
    {
        OnClick?.Invoke();
    }

    public void SetButtonState(bool isOn)
    {
        if (buttonToggleBehavior != null)
        {
            buttonToggleBehavior.SetState(isOn);
        }
    }
}
