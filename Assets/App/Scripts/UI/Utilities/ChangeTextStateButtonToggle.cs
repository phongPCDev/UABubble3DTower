using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeTextStateButtonToggle : BaseButtonToggle, IButtonToggleBehavior
{
    [SerializeField] private Text textState;

    protected override void Awake()
    {
        base.Awake();

        SetButtonToggleBehavior(this);
    }

    public void SetState(bool isOn)
    {
        textState.text = isOn ? "ON" : "OFF";
    }
}
