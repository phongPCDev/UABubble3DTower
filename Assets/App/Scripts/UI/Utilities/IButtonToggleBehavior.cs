using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IButtonToggleBehavior
{
    public void SetState(bool isOn);
}
