using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseGroupButtonToggle : BaseButtonToggle, IButtonToggleBehavior
{
    private GameObject groupOn;
    private GameObject groupOff;

    protected override void Awake()
    {
        base.Awake();

        SetButtonToggleBehavior(this);
    }

    public void SetState(bool isOn)
    {
        groupOn.SetActive(isOn);
        groupOff.SetActive(!isOn);
    }
}
