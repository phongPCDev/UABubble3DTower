﻿using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIButtonScaleOnTouch : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler, IPointerEnterHandler
{
    public static Vector3 DefaultScaleValue => new Vector3(0.95f, 0.95f, 0.95f);
    [SerializeField] private Transform target;
    [SerializeField][OnValueChanged(nameof(OnScaleTypeChanged))] private ScaleType scaleType;
    [SerializeField][ShowIf(nameof(IsCustomValue))] private Vector3 scaleValue = new Vector3(0.95f, 0.95f, 0.95f);

    private float scaleDuration = 0.05f;
    private Button button;
    private Vector3 baseScale;
    private bool onHold;

    private bool IsCustomValue => scaleType == ScaleType.Custom;
    private string tweenID = string.Empty;

    private void OnValidate()
    {
        if (target == null)
            target = transform;
    }

    private void Awake()
    {
        button = GetComponent<Button>();
        SetTarget(target);
    }

    private void OnDisable()
    {
        onHold = false;
        StopScaleTween();
        target.localScale = baseScale;
    }

    public void SetTarget(Transform target)
    {
        if (target == null)
            target = transform;
        this.target = target;
        tweenID = target.GetInstanceID() + "_scaleOnTouch";
        baseScale = target.localScale;
        OnScaleTypeChanged();
    }

    public Transform GetTarget()
    {
        return target;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onHold = true && button.interactable && button.enabled;
        if (onHold)
        {
            StopScaleTween();
            var targetScale = new Vector3(baseScale.x * scaleValue.x, baseScale.y * scaleValue.y, baseScale.z * scaleValue.z);
            target.DOScale(targetScale, scaleDuration).SetEase(Ease.Linear).SetUpdate(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //if (onHold)
        //{
        //    StopScaleTween();
        //    target.DOScale(baseScale, scaleDuration).SetEase(Ease.Linear).SetUpdate(true);
        //}
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onHold = false;
        StopScaleTween();
        target.DOScale(baseScale, scaleDuration).SetEase(Ease.Linear).SetUpdate(true);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //if (onHold && !inPlayAnimationWhenClick)
        //{
        //    StopScaleTween();
        //    target.DOScale(scaleValue, scaleDuration).SetEase(Ease.Linear).SetUpdate(true);
        //}
    }

    private void OnScaleTypeChanged()
    {
        scaleValue = GetScaleValue();
    }

    private Vector3 GetScaleValue()
    {
        if (scaleType == ScaleType.Default)
            return DefaultScaleValue;
        else
            return scaleValue;
    }

    private void StopScaleTween()
    {
        if (!string.IsNullOrEmpty(tweenID))
            DOTween.Kill(tweenID);
    }

    public enum ScaleType
    {
        Default,
        Custom,
    }
}
