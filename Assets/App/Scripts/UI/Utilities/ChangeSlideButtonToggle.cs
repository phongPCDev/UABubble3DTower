using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSlideButtonToggle : BaseButtonToggle, IButtonToggleBehavior
{
    [SerializeField] private Slider slider;

    protected override void Awake()
    {
        base.Awake();

        SetButtonToggleBehavior(this);
    }

    public void SetState(bool isOn)
    {
        slider.value = isOn ? 1 : 0;
    }
}
