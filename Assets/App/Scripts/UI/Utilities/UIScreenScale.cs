using AFramework.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class UIScreenScale : MonoBehaviour
{
    public static float BaseScaleValue = 1.0f;

    [SerializeField] Vector2 baseResolution = new Vector2(1080, 1920);
    [SerializeField] Vector2 scaleClamp = new Vector2(1, 1.21875f);
    [SerializeField] CanvasScaler canvasScaler;
    Vector2 lastResolution;
    private void Awake()
    {
#if TOOL_VERSION
        canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.referenceResolution = new Vector2(1080, 1920);
#else
        UpdateScale();
#if !UNITY_EDITOR
        if (Application.isPlaying)
        {
            Destroy(this);
        }
#else
        UnityEditor.EditorApplication.update -= UpdateScale;
        UnityEditor.EditorApplication.update += UpdateScale;
#endif
#endif
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        UpdateScale();
    }

    private void OnDestroy()
    {
        UnityEditor.EditorApplication.update -= UpdateScale;
    }
#endif

    void UpdateScale()
    {
#if FIRST_RELEASE
        canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.referenceResolution = new Vector2(1080, 1920);
        return;
#else
        // canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
#endif

        if (canvasScaler.uiScaleMode != CanvasScaler.ScaleMode.ConstantPixelSize)
            return;
        var currentResolution = GetGameResolution();
        if (lastResolution == currentResolution) return;
        float minBase = Mathf.Min(baseResolution.x, baseResolution.y);
        float maxBase = Mathf.Max(baseResolution.x, baseResolution.y);
        float baseRatio = maxBase / minBase;

        float minDevice = Mathf.Min(currentResolution.x, currentResolution.y);
        float maxDevice = Mathf.Max(currentResolution.x, currentResolution.y);
        float deviceRatio = maxDevice / minDevice;

        //float resultScale = Mathf.Clamp(deviceRatio / baseRatio, scaleClamp.x, scaleClamp.y);
        if (deviceRatio > 1.77f)
        {
            canvasScaler.scaleFactor = (minDevice / minBase) * Mathf.Clamp(deviceRatio / baseRatio, scaleClamp.x, scaleClamp.y);
            BaseScaleValue = canvasScaler.scaleFactor / (minDevice / minBase);
        }
        else
        {
            canvasScaler.scaleFactor = maxDevice / maxBase;
            BaseScaleValue = 1;
        }
        lastResolution = currentResolution;

#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            this.gameObject.SetActive(false);
            this.gameObject.SetActive(true);
        }
#endif
    }

    public static Vector2 GetGameResolution()
    {
#if UNITY_EDITOR
        string[] res = UnityEditor.UnityStats.screenRes.Split('x');
        return new Vector2(int.Parse(res[0]), int.Parse(res[1]));
#else
        return new Vector2(Screen.width, Screen.height);
#endif
    }

    public static void FixBottomUIScale(RectTransform rectTrans, bool childInvert, bool hasSetWidthWhenIpad = true)
    {
        if (rectTrans == null)
        {
            //ADebug.Log($"UIScreenScale: Null RectTrans");
            return;
        }
        var resolution = GetGameResolution();
        if (Mathf.Max(resolution.x, resolution.y) / Mathf.Min(resolution.x, resolution.y) > 1.77f)
        {
            if (childInvert)
            {
                rectTrans.localScale = Vector3.one * UIScreenScale.BaseScaleValue;
            }
            else
            {
                rectTrans.localScale = Vector3.one * (1 / UIScreenScale.BaseScaleValue);
            }
        }
        else
        {
            if (hasSetWidthWhenIpad)
            {
                var size = rectTrans.sizeDelta;
                size.x = AFramework.UI.CanvasManager.UIRectTrans.rect.width;
                rectTrans.sizeDelta = size;
            }
        }
    }

    public static float GetUIScale(bool childInvert)
    {
        float returnScale = 1;

        var resolution = GetGameResolution();
        if (Mathf.Max(resolution.x, resolution.y) / Mathf.Min(resolution.x, resolution.y) > 1.77f)
        {
            if (childInvert)
            {
                returnScale = UIScreenScale.BaseScaleValue;
            }
            else
            {
                returnScale = (1 / UIScreenScale.BaseScaleValue);
            }
        }

        return returnScale;
    }

    #region Scale By Parent

    public static void ScaleUIToParentByHeight(RectTransform rectTrans)
    {
        float scale = rectTrans.parent.GetComponent<RectTransform>().rect.height / 1920f;
        rectTrans.localScale = Vector3.one * scale;
    }

    public static void ScaleUIToParentBySize(RectTransform rectTrans)
    {
        Rect rect = rectTrans.parent.GetComponent<RectTransform>().rect;
        float rectRatio = rect.width / rect.height;
        if (rectRatio >= 0.5625f)
        {
            float scale = rectRatio / 0.5625f;
            rectTrans.localScale = Vector3.one * scale;
        }
        else
        {
            ScaleUIToParentByHeight(rectTrans);
        }
    }

    #endregion

    #region Scale By Screen Size

    public static void ScaleUIByScreenHeight(RectTransform rectTrans)
    {
        float scale = CanvasManager.UIRectTrans.rect.height / 1920f;
        rectTrans.localScale = Vector3.one * scale;
    }


    public static void ScaleUIByScreenSize(RectTransform rectTrans)
    {
        float rectRatio = (float)CanvasManager.UIRectTrans.rect.width / CanvasManager.UIRectTrans.rect.height;
        if (rectRatio >= 0.5625f)
        {
            float scale = rectRatio / 0.5625f;
            rectTrans.localScale = Vector3.one * scale;
        }
        else
        {
            ScaleUIByScreenHeight(rectTrans);
        }
    }

    #endregion
}
