using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// https://forum.unity.com/threads/content-size-fitter-refresh-problem.498536/
/// Inspired by 'look001'
/// Used for updating all LayoutGroup/ContentSizeFitter in 1 frame
/// Attach the script to the upper-most component 'LayoutGroup/ContentSizeFitter'
/// </summary>
public class LayoutGroupRefresher : MonoBehaviour
{
    private List<(RectTransform, LayoutGroup, ContentSizeFitter)> cacheList = new();

    public void Refresh()
    {
        if (cacheList.Count > 0)
        {
            RefreshFromCache();
        }
        else
        {
            RefreshAllRect(transform as RectTransform);
        }
    }

    private void RefreshAllRect(RectTransform rect)
    {
        if (rect == null) return;

        // Recursion
        foreach (var child in rect)
        {
            RefreshAllRect(child as RectTransform);
        }

        // Add to cache
        LayoutGroup layoutGroup = rect.GetComponent<LayoutGroup>();
        ContentSizeFitter contentSizeFitter = rect.GetComponent<ContentSizeFitter>();
        if (layoutGroup != null || contentSizeFitter != null)
        {
            cacheList.Add((rect, layoutGroup, contentSizeFitter));
        }

        // Refresh
        if (!rect.gameObject.activeSelf) return;

        if (layoutGroup != null)
        {
            layoutGroup.SetLayoutHorizontal();
            layoutGroup.SetLayoutVertical();
        }

        if (contentSizeFitter != null)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
        }
    }

    private void RefreshFromCache()
    {
        foreach (var cache in cacheList)
        {
            if (cache.Item1 == null || !cache.Item1.gameObject.activeSelf) continue;

            // Layout
            if (cache.Item2 != null)
            {
                cache.Item2.SetLayoutHorizontal();
                cache.Item2.SetLayoutVertical();
            }

            // Content size fitter
            if (cache.Item3 != null)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(cache.Item1);
            }
        }
    }
}