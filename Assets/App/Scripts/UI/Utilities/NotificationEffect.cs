﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace AFramework.GameNotice
{
    public class NotificationEffect : MonoBehaviour
    {
        public Vector3 ScaleFrom = Vector3.one;
        public Vector3 ScaleTo = Vector3.one * 1.2f;
        public float ScaleTime = 0.8f;

        Sequence mEffect;

        private void OnEnable()
        {
            transform.localScale = ScaleFrom;
            mEffect = DOTween.Sequence();
            mEffect.Append(transform.DOScale(ScaleTo, ScaleTime));
            mEffect.Append(transform.DOScale(ScaleFrom, ScaleTime));
            mEffect.SetLoops(-1);
        }

        private void OnDisable()
        {
            if (mEffect != null)
            {
                mEffect.Kill();
                mEffect = null;
            }
        }
    }
}